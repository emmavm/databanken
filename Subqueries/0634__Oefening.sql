USE ModernWays;
SELECT MIN(PersoonlijkGemiddeldeCijfer) AS 'Gemiddelde'
FROM
(select avg(Cijfer) as 'PersoonlijkGemiddelde'
 from Evaluaties
 GROUP by Studenten_Id) as PersoonlijkGemiddeldes;