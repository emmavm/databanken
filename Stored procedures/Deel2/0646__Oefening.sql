use aptunes;
drop procedure if exists MockAlbumRelease

delimiter $$
create procedure MockAlbumRelease(in extraReleases int)
begin
declare counter int default 0;
declare succes bool;
	repeat
		call MockAlbumReleaseWithSucces(succes);
			if succes = 1 then
				 set teller = teller +1;
			end if;
		until teller = extraReleases
	end repeat;
end $$
delimiter ;

-- CALL MockAlbumReleases(20)

-- set MockAlbumReleaseWithSuccess = CONCAT(MockAlbumReleaseWithSuccess,teller,',');
-- 			set teller = teller + 1;
-- 		until teller >= 20