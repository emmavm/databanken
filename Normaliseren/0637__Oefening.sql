use ModernWays;
-- STAP 1 --
-- creer een nieuwe tabel met de auteursgegevens
-- Voornaam en Familienaam
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null,
	Id int auto_increment primary key,
    -- Overige kolommen aan de tabel Personen toevoegen
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null
);
				-- in apart docuement om te testen 
					-- De tabel Personen creëren op basis van de tabel Boeken
					insert into Personen (Voornaam, Familienaam)
					   select distinct Voornaam, Familienaam from Boeken;

					-- De foreign key kolom in de tabel Boeken invullen
					select Boeken.Voornaam as 'voornaam afkomstig uit Personen',
					   Boeken.Familienaam,
					   Boeken.Personen_Id,
					   Personen.Voornaam as 'voornaam afkomstig uit Personen',
					   Personen.Familienaam,
					   Personen.Id
					from Boeken cross join Personen
					where Boeken.Voornaam = Personen.Voornaam and
						Boeken.Familienaam = Personen.Familienaam;
  
-- De tabel Boeken en Personen linken
-- foreign key verplicht maken 
alter table Boeken add Personen_Id int not null;

set sql_safe_updates = 0;
update Boeken cross join Personen
	set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Persoon.Voornaam and
	Boeken.Familienaam = Personen.Familienaam;
set sql_safe_updates = 1;

-- Dubbele kolommen verwijderen uit de tabel Boeken
alter table Boeken drop column Voornaam,
    drop column Familienaam;

-- De foreign key constraint op de kolom Personen_Id toevoegen
-- dan de constraint toevoegen
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);
