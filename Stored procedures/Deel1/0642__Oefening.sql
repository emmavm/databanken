use aptunes;

delimiter $$
create procedure CleanupOldMemberships(in DeEinddatum date, out num int)
begin
start transaction;
select count(*) into num
from Lidmaatschappen
where Lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < DeEinddatum;
set sql_safe_updates = 0;
delete 
from Lidmaatschappen
where Lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < DeEinddatum;
set sql_safe_updates = 1;
commit;
end $$
delimiter ;