use aptunes;
drop procedure if exists DangerousInsertAlbumRelease 

delimiter $$
create procedure DangerousInsertAlbumRelease()
begin
	declare numberOfAlbums int default 0;
    declare numberOfBands int default 0;
    declare randomBandId1 int default 0;
    declare randomAlbumId1 int default 0;
    declare randomBandId2 int default 0;
    declare randomAlbumId2 int default 0;
    declare randomBandId3 int default 0;
    declare randomAlbumId3 int default 0;
    declare exit handler for sqlexeption
    
    begin
		rollback;
        select 'Nieuwe releases konden niet worden toegevoegd';
    end;
    
    select count(*) 
    into numberOfAlbums from Albums;
	select count(*) 
    into numberOfBands from Bands;
    
    set randomAlbumId1 = floor(rand() * numberOfAlbums) + 1;
	set randomAlbumId2 = floor(rand() * numberOfAlbums) + 1;
    set randomAlbumId3 = floor(rand() * numberOfAlbums) + 1;
    set randomBandId1 = floor(rand() * numberOfBands) + 1;
    set randomAlbumId2 = floor(rand() * numberOfBands) + 1;
    set randomAlbumId3 = floor(rand() * numberOfBands) + 1;
    start transaction;
     insert into AlbumReleases(Bands_Id, Albums_Id)
		values
			(randomBandId1, randomAlbumId1),
            (randomBandId2, randomAlbumId2);
		set randomValue = floor(rand() * 3) +1;
        if randomValue = 1 then
			signal sqlstate '45000';
		end if;
	insert into albumrelease(Bands_Id, Albums_Id)
		values
			(randomBandId3, randomAlbumId3);
	commit;
    end$$
    delimiter ;
    
-- call DangerousInsertAlbumRelease();