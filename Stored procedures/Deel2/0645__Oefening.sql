use aptunes;
drop procedure if exists MockAlbumReleaseWithSucces;


delimiter $$
create procedure MockAlbumReleaseWithSucces(out succes bool)
begin
declare numberOfAlbums int default 0;
declare numberOfBands int default 0;
declare randomAlbumId int default 0;
declare randomBandId int default 0;

select count(*) 
into numberOfAlbums from Albums;

select count(*) 
into numberOfBands from Bands;

set randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
set randomBandId = FLOOR(RAND() * numberOfBands) + 1;

if (randomBandId, randomAlbumsId) not in (select * from Albumreleases) then
	insert into Albumreleases(Bands_Id, Albums_Id)
	values (randomBandId, randomAlbumId);
    set succes = 1;
else 
	set succes = 0;
end if;
end $$

delimiter ;

-- call MockAlbumReleaseWithSucces(@succes);
-- select @succes;