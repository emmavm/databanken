use aptunes;

delimiter $$
create procedure GetAlbumDuration(in album int, out totalDuration smallint unsigned)
begin
	declare songDuration tinyint unsigned default 0;
    declare ok bool default false; -- bool kan dus ook in de plaats van een int
    declare songDurationCursor cursor for select Lengte from Lieddjes where Albums_Id = album;
    declare continue handler for not found set ok = true;
    set totalDuration = 0;
    open songDurationCursor; -- open cursor
    fetchloop: loop
		fetch songDurationCursor into songDuration;  -- moet hier staan anders komt de duration te hoog uit
        if ok = True then 
			leave fetchloop;
        end if ;
        set totalDuration = totalDuration + songDuration;  
	end loop;
    close songDurationCursor; -- sluit de cursor
end$$ 
delimiter ;