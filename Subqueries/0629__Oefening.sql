USE ModernWays;

SELECT DISTINCT s.Id
FROM Studenten s
INNER JOIN Evaluaties e ON s.id = e.Studenten_Id
GROUP BY s.Id
HAVING 
AVG(e.Cijfer)  >
(SELECT AVG(cijfer) FROM Evaluaties);

-- (SELECT AVG(Cijfer) FROM Evaluaties GROUP BY Studenten_Id) 
